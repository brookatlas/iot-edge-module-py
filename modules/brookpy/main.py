# Copyright (c) Microsoft. All rights reserved.
# Licensed under the MIT license. See LICENSE file in the project root for
# full license information.

import time
import os
import sys
import asyncio
import json
from bleak import discover, BleakClient
from six.moves import input
from datetime import datetime
import threading
import bleak
import requests
from azure.iot.device.aio import IoTHubModuleClient
from azure.iot.device import Message, IoTHubDeviceClient


global TEMP_UUID
global PRESSURE_UUID

TEMP_UUID = "6c366661-0ceb-4799-823b-f158ec1d0de5"
PRESSURE_UUID = "b2e1cc6e-e0fb-47ab-a9b8-0d87fae86db5"


# define behaviour for bluetooth listener
async def discoverDevices():
    devices = await discover()
    deviceList = []
    for device in devices:
        deviceList.append(device)
    return deviceList


def getDeviceAddressByName(name, devices):
    for device in devices:
        if (device.name == name):
            return device.address

async def main():
    try:
        if not sys.version >= "3.5.3":
            raise Exception( "The sample requires python 3.5.3+. Current version of Python: %s" % sys.version )
        print ( "IoT Hub Client for Python" )

        # The client object is used to interact with your Azure IoT hub.
        #module_client = IoTHubModuleClient.create_from_edge_environment()

        # connect the client.
        #await module_client.connect()
        conn_string = os.getenv('edgeHubConnectionString')

        client = IoTHubDeviceClient.create_from_connection_string(conn_string)





        # define behavior for halting the application
        def stdin_listener():
            while True:
                try:
                    selection = input("Press Q to quit\n")
                    if selection == "Q" or selection == "q":
                        print("Quitting...")
                        break
                except:
                    time.sleep(10)

        async def bluetooth_listener(client,loop):
            devices = await discover()
            address = None
            deviceList = []
            for device in devices:
                deviceList.append(device)
            for device in devices:
                if(device.name == 'x'):
                    address = device.address
            async with BleakClient(address, loop) as btClient:
                while True:
                    temp = await btClient.read_gatt_char(TEMP_UUID)
                    pressure = await btClient.read_gatt_char(PRESSURE_UUID)
                    temp = int.from_bytes(temp, byteorder='little')
                    pressure = int.from_bytes(pressure, byteorder='little')
                    dataJson = {'machine':{'temperature': temp, 'pressure': pressure}, 'ambient': {'temperature':temp, 'humidity':0}, 'timeCreated':datetime.now().strftime("%Y-%m-%d-%H:%M:%S")}
                    dataJson = json.dumps(dataJson)
                    message = Message(dataJson)
                    print("sending message to hub: {}".format(dataJson))
                    client.send_message(message)
                    time.sleep(1)
        print ( "The sample is now waiting for messages. ")

        # Run the stdin listener in the event loop
        loop = asyncio.get_event_loop()
        user_finished = loop.run_in_executor(None, stdin_listener)
        output = await bluetooth_listener(client, loop)

        # Wait for user to indicate they are done listening for messages
        await user_finished

        # Cancel listening
        #listeners.cancel()


        # Finally, disconnect
        #await module_client.disconnect()

    except Exception as e:
        print ( "Unexpected error %s " % e )
        raise

global loop
global address
if __name__ == "__main__":
    global loop
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()

    # If using Python 3.7 or above, you can use following code instead:
    # asyncio.run(main())